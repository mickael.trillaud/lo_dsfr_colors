# LO DSFR Colors
LibreOffice DSFR Colors (lo_dsfr_colors) est une extension simple pour LibreOffice permettant d'installer une palette de couleurs utilisant les couleurs de la charte graphique et du système de design de l'État Français (<https://www.info.gouv.fr/marque-de-letat/les-couleurs>, <https://www.systeme-de-design.gouv.fr/>).

## Description
La palette importée comporte les jeux de couleurs suivants :
- les couleurs de la charte graphique de l'État, version à jour au 1er septembre 2024 : Bleu France, Blanc, Rouge Marianne ainsi que les teintes principals dans leurs trois déclinaisons : Macaron, Tuile, Glycine, Cumulus, Écume, Archipel, Menthe, Émeraude, Bourgeon, Tilleul verveine, Tournesol, Moutarde, Terre battue, Café crème, Caramel, Opéra, Gris galet ;
- une extension de ces teintes dans une gamme de luminosité/saturation allant de lightest à darkest sélectionnées dans les déclinaisons du dsfr ;
- une gamme allant du blanc au noir avec une déclinaison de gris sélectionnés dans le dsfr ;
- les couleurs de la charte graphique dans sa version précédente (2017) avec les teintes A à R (pour chaque teinte, la déclinaison 1 est la teinte de base, la déclinaison 0 est à +10% de noir, et les teintes 2, 3 et 4 respectivement à 50%, 25% et 10% de saturation). La teinte non numérotée est un ajout permettant de compléter la gamme dans les tons sombres ;
- les 3 couleurs fluo définies dans la charte graphique de l'État : rose, jaune, vert.

La palette, au format GPL, est également utilisable dans d'autres applications comme QGIS ou GIMP.

## Utilisation
Installez l'extension dans LibreOffice, redémarrez l'application puis sélectionnez la palette «Marianne» lors de vos choix de couleurs.
L'usage de la charte graphique et du système de design de l'État Français doit respecter les conditions légales mentionnées à cette adresse : <https://www.systeme-de-design.gouv.fr/a-propos/conditions-generales-d-utilisation>.

## Auteur
- **Mickaël Trillaud** <mickael.trillaud@agriculture.gouv.fr>

## Licence
CC0 1.0 Universal
Cette extension est libre de droit et prévue pour un usage dans le domaine public. Elle est mise à disposition sous licence Creative Common 0 "No rights reserved".
<https://creativecommons.org/publicdomain/zero/1.0/>
